<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Skill extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'title', 'degree',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }

    public function file()
    {
        return $this->morphOne(File::class,'fileable');
    }
}
