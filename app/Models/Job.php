<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Job extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'title', 'company_name', 'start_at', 'end_at', 'inWork', 'description',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }


}
