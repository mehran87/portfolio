<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name', 'family', 'mobile', 'address', 'role', 'email', 'email_verified_at', 'password', 'instagram', 'telegram', 'facebook', 'linkedin', 'remember_token',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'social' => 'array'
    ];

    public function file(): MorphOne
    {
        return $this->morphOne(File::class,'fileable');
    }

    public function jobs(): HasMany
    {
        return $this->hasMany(Job::class , 'user_id' , 'id');
    }

    public function blogs(): HasMany
    {
        return $this->hasMany(Blog::class, 'user_id' , 'id');
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class,'user_id','id');
    }

    public function skills(): HasMany
    {
        return $this->hasMany(Skill::class,'user_id','id');
    }

    public function studies(): HasMany
    {
        return $this->hasMany(Study::class , 'user_id','id');
    }

}
