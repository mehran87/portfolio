<?php

namespace App\Http\Services\User;

use App\Http\Services\BaseService;
use App\Http\Services\File\FileService;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserService extends BaseService
{

    protected string $table = 'users';

    public function __construct(User $user,private FileService $fileService)
    {
        parent::__construct($user);
    }


}
