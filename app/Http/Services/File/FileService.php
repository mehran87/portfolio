<?php

namespace App\Http\Services\File;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class FileService
{

    protected string $table = 'files';

    public function __construct(File $file)
    {
        parent::__construct($file);
    }


}
