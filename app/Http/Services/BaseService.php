<?php

namespace App\Http\Services;

use Illuminate\Database\Eloquent\Model;

class BaseService
{

    public function __construct(private Model $model)
    {
    }

    public function index($model,$with,$orders)
    {
        return $model::with($with)->orderBy($orders)->get();
    }

    public function store(array $payload = []): Model
    {
        return $this->model->create($payload);
    }

    public function show($model)
    {
        return $model;
    }

    public function update($model,array $payload = []): Model
    {
        $model->update($payload);
        return $model;
    }

    public function destroy($model): bool
    {
        return $model->delete();
    }
}
