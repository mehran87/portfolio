<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'name'   => ['required'],
            'family' => ['required'],
            'address' => ['nullable'],
            'image'  => ['nullable','sometimes', 'image', 'max:2048'],
        ];
        if ($this->method() == self::METHOD_PATCH) {
            $rules['mobile'] = ['required', 'min:11', 'unique:users,mobile,' . $this->user->id];
            $rules['email'] = ['required', 'email', 'unique:users,email,' . $this->user->id];
            $rules['instagram'] = ['nullable','unique:users,instagram,'. $this->user->id];
            $rules['telegram'] = ['nullable','unique:users,telegram,'. $this->user->id];
            $rules['facebook'] = ['nullable','unique:users,facebook,'. $this->user->id];
            $rules['linkedin'] = ['nullable','unique:users,linkedin,'. $this->user->id];
            $rules['role'] = ['nullable'];
        }

        if ($this->method() == self::METHOD_POST) {
            $rules['mobile'] = ['required', 'min:11', 'unique:users,mobile'];
            $rules['email'] = ['required', 'email', 'unique:users,email'];
            $rules['instagram'] = ['nullable','unique:users,instagram'];
            $rules['telegram'] = ['nullable','unique:users,telegram'];
            $rules['facebook'] = ['nullable','unique:users,facebook'];
            $rules['linkedin'] = ['nullable','unique:users,linkedin'];
            $rules['password'] = ['required' ,'min:6'];
            $rules['role'] = ['nullable'];
        }

        return $rules;

    }
}
