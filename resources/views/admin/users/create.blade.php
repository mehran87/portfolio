@extends('admin.layouts.main')

@section('content')
    <div class="container mt-5">
        <form method="post" action="{{isset($user) ? route('admin.user.update',$user->id) : route('admin.user.store') }}">
            @csrf
           @if(isset($user)) @method('PATCH') @endif
            <!-- row -->
            <div class="row tm-content-row">
                <div class="tm-block-col tm-col-avatar">
                    <div class="tm-bg-primary-dark tm-block tm-block-avatar">
                        <h2 class="tm-block-title" style="text-align: right">انتخاب تصویر</h2>
                        <div class="tm-avatar-container">
                            <img
                                src="{{URL::asset('admin_assets/img/avatar.png')}}"
                                alt="Avatar"
                                class="tm-avatar img-fluid mb-4"
                            />
                            <a href="#" class="tm-avatar-delete-link">
                                <i class="far fa-trash-alt tm-product-delete-icon"></i>
                            </a>
                        </div>
                        <x-forms.input  name="image" type="file"/>
                    </div>
                </div>
                <div class="tm-block-col tm-col-account-settings">
                    <div class="tm-bg-primary-dark tm-block tm-block-settings">
                        <h2 class="tm-block-title mb-5" style="text-align: center">ثبت اطلاعات کاربر</h2>
                        <div class="tm-signup-form row">

                            <x-forms.input style="text-align:right" class="col-lg-6" name="name" id="name" type="text"
                                           label="نام" value="{{isset($user) ? $user->name : ''}}"/>
                            <x-forms.input style="text-align:right" class="col-lg-6" name="family" id="family"
                                           type="text" label="نام خانوادگی" value="{{isset($user) ? $user->family : ''}}"/>
                            <x-forms.input style="text-align:right" class="col-lg-6" name="mobile" id="mobile"
                                           type="text" label="تلفن همراه" value="{{isset($user) ? $user->mobile : ''}}"/>
                            <x-forms.input style="text-align:right" class="col-lg-6" name="email" id="email"
                                           type="email" label="پست الکترونیک" value="{{isset($user) ? $user->email : ''}}"/>
                            <x-forms.input style="text-align:right" class="col-lg-6" name="password" id="password"
                                           type="password" label="کلمه عبور" />
                            <x-forms.input style="text-align:right" class="col-lg-6" name="confirmPassword"
                                           id="confirmPassword" type="password" label="تکرار کلمه عبور" />

                            <x-forms.input style="text-align:right" class="col-12" name="address" id="address"
                                           type="text" label="آدرس" value="{{isset($user) ? $user->address : ''}}"/>
                            <x-forms.input style="text-align:left" class="col-12" name="facebook" id="facebook"
                                           type="text" label="Facebook" value="{{isset($user) ? $user->facebook : ''}}"/>
                            <x-forms.input style="text-align:left" class="col-12" name="telegram" id="telegram"
                                           type="text" label="Telegram" value="{{isset($user) ? $user->telegram : ''}}"/>
                            <x-forms.input style="text-align:left" class="col-12" name="instagram" id="instagram"
                                           type="text" label="Instagram" value="{{isset($user) ? $user->instagram : ''}}"/>
                            <x-forms.input style="text-align:left" class="col-12" name="linkedin" id="linkedin"
                                           type="text" label="Linkedin" value="{{isset($user) ? $user->linkedin : ''}}"/>

                            <div class="col-12">
                                <x-forms.button button-style="success" title="{{isset($user) ? 'بروزرسانی پروفایل' : 'ثبت کاربر'}}" />
                            </div>
                        </form>
                        @isset($user)
                            <div class="col-12 mt-3">
                                <x-forms.button button-style="danger" title="حذف حساب کاربری"/>
                            </div>
                        @endisset
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
