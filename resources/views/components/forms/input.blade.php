<div class="form-group {{$class}}" style="{{$style}}">
    <label for="{{$id}}">{{$label}}</label>
    <input
        id="{{$id}}"
        name="{{$name}}"
        type="{{$type}}"
        placeholder="{{$placeholder}}"
        value="{{$value ?? old($name)}}"
        class="form-control validate"
    />
    <div class=" mt-1">
        @error($name)
        <span class="badge badge-primary">
                    <small class="form-text mt-0">{{$message}}</small>
                </span>
        @enderror
    </div>
</div>
