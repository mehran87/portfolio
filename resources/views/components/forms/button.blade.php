<button
    type="submit"
    class="btn btn-{{$buttonStyle}} btn-block text-uppercase">
    {{$title}}
</button>
